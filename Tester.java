import java.util.Iterator;

public class Tester {
    public static void main(String[] args) {
        DropOutStackArray<String> stack = new DropOutStackArray<String>();
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");
        stack.push("E");
        stack.push("F");
        stack.push("G");
        stack.push("H");
        stack.push("I");
        stack.push("J");
        stack.push("K");
        stack.push("L");
        
        Iterator<String> i = stack.iterator();
        while (i.hasNext()) {
            String s = i.next();
            if (s == "C") {
                i.remove();
            }
        }
        System.out.println(stack.toString());

        
        //DropOutStackLinkedList<String> stack2 = new DropOutStackLinkedList<String>(); 
        //System.out.println(stack2.toString());
    }
}
