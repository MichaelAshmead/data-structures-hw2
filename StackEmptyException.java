/**
 * @author Michael Ashmead
 */
public class StackEmptyException extends Exception {
    /**
     * No idea what this is for???
     */
    private static final long serialVersionUID = 1L;

    /**
     * This is the exception constructor and message.
     */
    public StackEmptyException() {
        super("There's nothing in the stack!");
    }
}