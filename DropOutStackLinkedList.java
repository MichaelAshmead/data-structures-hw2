import java.util.Iterator;

/**
 * @author Michael Ashmead
 * @param <T> is the object type to be stored in the stack.
 */
public class DropOutStackLinkedList<T> implements DropOutStack<T> {
    /**
     * Default size of stack.
     */
    private static final int DEFAULT_CAP = 10;
    /**
     * Max number of elements in stack.
     */
    private int cap;
    
    /**
     * Current number of elements in the stack.
     */
    private int size;
    
    /**
     * Pointer node to the first node in the stack.
     */
    private Node head;
    
    /**
     * Trailing node after the last node in the stack.
     */
    private Node tail;
    /**
     * @author Michael Ashmead
     */
    private class Node {
        /**
         * Data to be stored in node.
         */
        private T data = null;
        /**
         * Node before current Node.
         */
        private Node next = null;
        /**
         * Node after current Node.
         */
        private Node prev = null;
    }
    
    /**
     * Constructor that creates a head and a tail pointer nodes.
     */
    public DropOutStackLinkedList() {
        this.cap = this.DEFAULT_CAP;
        this.head  = new Node();
        this.tail = new Node();
        this.head.next = this.tail;
        this.tail.prev = this.head;
        this.size = 0;
    }
    /**
     * @param num is the specified size of the stack.
     */
    public DropOutStackLinkedList(int num) {
        this.cap = num;
        this.head  = new Node();
        this.tail = new Node();
        this.head.next = this.tail;
        this.tail.prev = this.head;
        this.size = 0;
    }

    @Override
    public Iterator<T> iterator() { // need to finish this method
        Iterator<T> iter = new Iterator<T>() {

            private int nextObject = 0;

            public boolean hasNext() {
                if (this.nextObject < size) {
                    return true;
                }
                return false;
            }

            public T next() {
                if (hasNext()) {
                    for (int x = 0; x < )
                    this.nextObject++;
                    Node current = new Node();
                    
                }
                return null;
            }

            public void remove() {
                
            }
        };
        return iter;
    }

    /**
     * Add object to top of stack.
     * @param data is what is to be added to top of stack.
     */
    public void push(T data) {
        Node temp = new Node();
        temp.data = data;
        temp.prev = this.head;
        this.head.next.prev = temp;
        temp.next = this.head.next;
        this.head.next = temp;
        if (this.size == 0) {
            this.tail.prev = temp;
        }
        if (this.size > this.cap) {
            this.tail.prev = this.tail.prev.prev;
            this.tail.prev.prev.next = this.tail;
        }
        else {
            size++;
        }
    }
    
    /**
     * @return T which is the object you just removed from the top of the stack.
     */
    public T pop() {
        if (this.size == 0) {
            try {
                throw new StackEmptyException();
            } catch (StackEmptyException e) {
                e.printStackTrace();
            }
            return null;
        }
        this.size--;
        T data = this.head.next.data;
        this.head.next.next.prev = this.head;
        this.head.next = this.head.next.next;
        return data;
    }

    /**
     * @return T which is the top object in the stack.
     */
    public T peek() {
        if (this.size == 0) {
            try {
                throw new StackEmptyException();
            } catch (StackEmptyException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return this.head.next.data;
        }
    }

    /**
     * @return an int that is the number of objects in the stack.
     */
    public int size() {
        return this.size;
    }

    /**
     * @return boolean that tells you if the stack has any objects in it or not.
     */
    public boolean isEmpty() {
        if (this.size > 0) {
            return false;
        }
        return true;
    }

    /**
     * @return String which is the stack in String
     */
    public String toString() {
        String string = new String();
        if (this.size != 0) {
            Node node = this.head.next;
            while (node.next != null) {
                string = (node.data + ", " + string);
                node = node.next;
            }
            string = string.substring(0, string.length() - 2);
        }
        return "[" + string + "]";
    }
}