import java.util.Iterator;

/**
 * @author Michael Ashmead
 * @param <T>
 */
public class DropOutStackArray<T> implements DropOutStack<T> {
    /**
     * Default max number of objects that can be stored in the stack is 10.
     */
    private static final int DEFAULT_CAP = 10;
    
    /**
     * Max number of objects that can be stored in the stack.
     */
    private int cap;
    
    /**
     * Current number of objects that are stored in the stack.
     */
    private int size;
    
    /**
     * This is the pointer for the bottom element of the stack.
     */
    private int rear;
    
    /**
     * This is the pointer for the top element of the stack.
     */
    private int front;
    
    /**
     * This is the stack for storing objects.
     */
    private T[] stack;
    
    /**
     * Create an empty stack.
     */
    @SuppressWarnings("unchecked")
    public DropOutStackArray() {
        this.size = 0;
        this.front = 0;
        this.rear = 0;
        this.cap = DEFAULT_CAP;
        this.stack = (T[]) new Object[this.cap];
    }
    
    /**
     * Create a stack with specified size.
     * @param x is the size of the stack.
     */
    @SuppressWarnings("unchecked")
    public DropOutStackArray(int x) {
        this.size = 0;
        this.front = 0;
        this.rear = 0;
        this.cap = x;
        this.stack = (T[]) new Object[this.cap];
    }
    
    /**
     * The iterator class has 3 methods: hasNext(), next(), and remove().
     * @return Iterator<T>
     */
    public Iterator<T> iterator() {
        Iterator<T> iter = new Iterator<T>() {

            private int nextObject = 0;

            public boolean hasNext() {
                if (this.nextObject < size) {
                    return true;
                }
                return false;
            }

            public T next() {
                if (hasNext()) {
                    this.nextObject++;
                    if (rear + this.nextObject-1 < 10) {
                        return stack[rear + this.nextObject-1];
                    }
                    else {
                        return stack[(rear + this.nextObject-1) % 10];
                    }
                }
                return null;
            }

            public void remove() {
                System.out.println(rear);
                System.out.println(front);
                size--;
                T[] temp = (T[]) new Object[cap];
                for (int x = 0; x < cap; x++) {
                    if (x == (this.nextObject - 1)) {
                        continue;
                    }
                    if (x > (this.nextObject - 1)) {
                        temp[x - 1] = stack[x];
                    } else {
                        temp[x] = stack[x];
                    }
                }
                stack = temp;
                front--;
                if (front == -1) {
                    front = 9;
                }
                System.out.println(rear);
                System.out.println(front);
            }
        };
        return iter;
    }
    
    /**
     * Add object to top of stack.
     * @param data is what is to be added to top of stack.
     */
    public void push(T data) {
        this.stack[this.front] = data;
        this.front++;
        if (this.front > this.cap - 1) {
            this.front = 0;
        }
        if (this.size < this.cap) {
            this.size++;
        }
        else {
            this.rear++;
        }
    }

    /**
     * @return an int that is the number of objects in the stack.
     */
    public int size() {
        return this.size;
    }

    /**
     * @return boolean that tells you if the stack has any objects in it or not.
     */
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }
    
    /**
     * @return T which is the object you just removed from the top of the stack.
     */
    public T pop() {
        if (this.size == 0) {
            try {
                throw new StackEmptyException();
            } catch (StackEmptyException e) {
                e.printStackTrace();
            }
            return null;
        }
        this.size--;
        if (this.front == 0) {
            this.front = this.cap - 1;
        }
        else {
            this.front--;
        }
        if (this.front != 0) {
            return this.stack[this.front - 1];
        }
        else {
            return this.stack[0];
        }
    }

    /**
     * @return T which is the top object in the stack.
     */
    public T peek() {
        if (this.size == 0) {
            try {
                throw new StackEmptyException();
            } catch (StackEmptyException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            if (this.front != 0) {
                return this.stack[this.front - 1];
            } else {
                return this.stack[this.cap - 1];
            }
        }
    }
    
    /**
     * @return String which is the stack in String
     */
    public String toString() {
        String string = "[";
        if (this.front > this.rear && this.size != 0) {
            for (int x = this.rear; x < this.front; x++) {
                string += this.stack[x] + ", ";
            }
        } else if (this.size != 0) {
            for (int x = this.rear; x < this.cap; x++) {
                string += this.stack[x] + ", ";
            }
            for (int x = 0; x < this.front; x++) {
                string += this.stack[x] + ", ";
            }
        }
        
        if (this.size != 0) {
            string = string.substring(0, string.length() - 2);
        }
        return string + "]";
    }
}