/** Interface for a bounded generic drop-out stack.
    @param <T> the base type of the objects in the stack.
 */

//updated on 30 Sept 2014 to include <T> in extends clause below
public interface DropOutStack<T> extends Iterable<T> 
{

    /** Add an element to the top of the stack.
        If stack's capacity has been reached, the
        element on the bottom of stack is removed.
        @param data the element to add
    */
    public void push(T data);

    /** Remove the top element from the stack.
        @throws StackEmptyException
        @return the element that was removed
    */
    public T pop();

    /** Peek at the top element of the stack.
        @throws StackEmptyException
        @return the top
    */
    public T peek();

    /** Find out how many elements are in the stack.
        @return the size
    */
    public int size();

    /** Find out if the stack is empty.
        @return true if empty, false otherwise
    */
    public boolean isEmpty();

    /** Create a description of the stack contents formatted in [ ], 
        starting with the bottom as the leftmost element, to the top.
        For example, a stack of integers might be [2, 3, 10, 5, 0] where
        2 is the bottom element and 0 is the top-most element.
        @return the description
    */
    public String toString();

}